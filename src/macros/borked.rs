/// ## Macro for instantly returning a Result<_, Bork>.
/// 
/// ## Usage:
/// There is multiple ways to use this macro:
/// * `borked!("Message about Error")` Creates a [BaseBork](struct.BaseBork.html)
/// where the messsage specified is returned as the name.
/// * `borked!("Message", cause)` Where message is the same as above, and
/// The cause is the Error/Bork that 'caused' this one.  The caused message
/// will be returned by the cause fn.
/// * `borked!(TY => Error_Type)` Here the `TY => ` must be put in exactly as
/// seen, but the Error_Type is the __identifier__ of the type you would like
/// to be returned as a Bork.
/// * `borked!(TY => ErrorType, cause)` same as above, but now with a cause.
/// cause must be an actual instantiation and __not__ just an identifier, like
/// ErrorType.
/// * `borked!(TY => ErrorType, C => cause, field0:val, field1:val, ...)`
/// Here ErrorType and cause are the same as above, but the remainder of the 
/// arguments are the __names__ of fields in the ErrorType, and the val's are the
/// values to set those fields to.

#[macro_export]
macro_rules! borked{
    ($message:expr) => { 
            {
                use $crate::{BaseBork, BorkChain};
                let e = Box::new(BaseBork{name: $message.into(), cause: None});
                return Err(e);
    
            }
    };
    

    ($message:expr, $cause:expr) => {
            {
                use $crate::{BorkChain, BaseBork, BorkWithMsg};
                let mut e = BaseBork::msg($message.into());
                e.chain($cause);
                return Err(e);
            }
    };

    (TY => $new_err:ident) => {
            {
                use $crate::BorkChain;
                let e = $new_err::new().unwrap();
                return Err(e);
            }
    };

    
    (TY => $new_err:ident, $cause:expr) => {
            {
                use $crate::BorkChain;
                let mut e = $new_err::new().unwrap();
                e.chain($cause.into());
                return Err(e);
            }
    };

    (TY => $new_err:ident, C => $cause:ident, $($field:ident:$val:expr),*) => {
            {
                use $crate::BorkChain;
                let mut e = $new_err::new().unwrap();
                e.chain($cause.into());
                $( { e.$field = $val.into(); } )*
                return Err(e);
            }
    };
    

}


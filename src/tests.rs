use crate::{ErrChain, BaseError, BorkWith, ErrWithMsg};

use std::error::Error;
type Etype = Result<(), Box<dyn ErrChain>>;

fn borked_base()-> Etype{
    let e = crate::BaseError{name: "ASF".into(), cause: None};
    //let i = 9;
    //let bb = B{val: e};
    borked!("this is borked");
    //
    //return Err(Box::new(e));
    Ok(())
}

fn borked_base_w_err()-> Etype{
    borked!(ERR => BaseError);
    Ok(())
}

fn borked_std_err() -> Etype{
    let e = u32::from_str_radix("ASDFA", 10)?;
    Ok(())
}

fn multiple_borked() -> Etype{
    let e0 = borked_base().unwrap_err();
    borked!("Borked Twice?", e0);
    Ok(())
}

fn multi_borked_q() ->Etype{
    multiple_borked()?;
    Ok(())
}

fn bork_with() -> Etype{
    let e = borked_base().unwrap_err();
    let i = u32::from_str_radix("ASDFA", 10).bork_with(BaseError::msg("This is a message!"))?;
    Ok(())
}

#[test]
fn test_borked_base() {
    let b = borked_base().unwrap_err();
    let b_str = format!("{}", b);
    assert!(b_str == "Error: this is borked");
}

#[test] 
fn test_borked_chain(){
    let b = multiple_borked().unwrap_err();
    let b_str = format!("{}", b);
    assert!(b_str == "Error: Borked Twice? => Cause: [Error: this is borked]");
}

#[test]
fn test_std_err(){
    let e = borked_std_err().unwrap_err();
    let ne: &std::num::ParseIntError = e.downcast_ref().unwrap();
    assert!(Error::is::<std::num::ParseIntError>(ne));
}

#[test]
fn test_borked_base_err(){
    let e = borked_base_w_err().unwrap_err();
}

#[test]
fn test_borked_q(){
    let e = multi_borked_q().unwrap_err();
}

#[test]
fn test_bork_with(){
    let e = bork_with().unwrap_err();
    println!("latest: {}", e);
}


# Rust-Borked

### Error handling for rust.

This Does a very similar thing to existing projects like 
[failure - Rust](https://docs.rs/failure/0.1.8/failure/) 
or [error_chain](https://docs.rs/error-chain/0.12.4/error_chain/)
but I was never quite satisfied with the other implementations.


This was my first time really mucking about with Rust's Trait system, so 
if I did anything incrdably dumb, or if improvments could otherwise be made, 
I'd love to hear about it. :)

## Install
borked is on [crates.io](https://crates.io/crates/borked) so it can be added to `Cargo.toml`: 
```
[dependencies]
...
borked = "0.1.0"
```

### Example
This crate provides features for working with any error that implements the
`std::error::Error` Trait. But Types implementing `BorkChain` are most useful
with this crate.

```
#[macro_use]
extern crate borked;
use borked::*;

fn doing_stuff() -> Borkable<u32>{
	// Do something useful...
	// Encounter error...
	Bork!("Oh No!");
	// Never reached in this case.
	return Ok(0);
}

fn q_mark()-> Borkable<u32>{
	let e = u32::from_str_radix("Obviously_not_gonna_work", 10)?;
	return Ok(e);
}

fn with_bork() -> Borkable<u32>{
	let e = u32::from_str_radix("Obviously_not_gonna_work", 10)
		.bork_with(BaseBork::msg("AHHH It Didn't Work!"))?;
	return Ok(e+7);

}
```




